function GanttChart(data){
    var showOrder = data.showOrder;

    //Create table element
    var ganttChartEl = document.createElement('table');

    //Create table heading
    var ganttChartHeadingEl = document.createElement('thead');
    var ganttChartHeadingtrEl = document.createElement('tr');
    var i = 0;
    while(i<showOrder.length){
        var td = document.createElement('td');
        var textEl = document.createTextNode(showOrder[i].heading);
        td.appendChild(textEl);
        ganttChartHeadingtrEl.appendChild(td);
        i++;
    }

    //date part
    var ganttChartDatePartHeadingEl = document.createElement('td');
    ganttChartHeadingtrEl.appendChild(ganttChartDatePartHeadingEl);
    //TODO date heading


    ganttChartHeadingEl.appendChild(ganttChartHeadingtrEl);
    ganttChartEl.appendChild(ganttChartHeadingEl);

    //Create table body
    var info = data.info;
    var ganttChartBodyEl = document.createElement('tbody');
    var i = 0;
    while(i<info.length){
        var tr = document.createElement('tr');
        var j = 0;
        while(j<showOrder.length){
            var td = document.createElement('td');
            var text = info[i][showOrder[j].key];
            var textEl = document.createTextNode(text);

            td.appendChild(textEl);
            tr.appendChild(td);
            j++;
        }

        var td = document.createElement('td');
        tr.appendChild(td);
        ganttChartBodyEl.appendChild(tr);
        i++;
    }

    ganttChartEl.appendChild(ganttChartBodyEl);
    return ganttChartEl;
}